<?php
class ActiveRecord extends CActiveRecord
{
	public function behaviors()
	{
		return array(
			'withRelated'=>array(
				'class'=>'application.components.behaviors.WithRelatedBehavior',
			),
		);
	}

	public function save($runValidation=true,$attributes=null)
	{
		if (parent::save($runValidation,$attributes) === false) {
			throw new CException('Error during saving the model');
		} else {
			return true;
		}
	}

	public function truncateTable()
	{
		$this->getDbConnection()->createCommand()->truncateTable($this->tableName());
	}
}