<?php
class MyTimes
{
	/**
	 * Входные параметры могут передаваться в 2х режимах:
	 * @param $dta string Дата когда событие произошло|Дата когда наступит expired
	 * @param $dh int Кол-во дней (или часов если добавлен символ h справа от значения ex: '48h'), по истечению которых событие считается expired|Ноль
	 *
	 * @return bool|string Возвращает $expiredReturn в случае если событие истекло,
	 * либо кол-во оставшихся часов:минут до наступления expired
	 * @throws CException
	 */
	public static function expiresIn($dta, $dh, $expiredReturn=false, $formatReturn='H:i')
	{
		if (empty($dta) && empty($dh)) {
			throw new CException('Invalid args');
		}
		if (strpos($dh, 'h')!== false) {
			$diff = strtotime($dta) + $dh*60*60 - time();
		} else {
			$diff = strtotime($dta) + $dh*24*60*60 - time();
		}
		if ($diff > 0) {
			$hours = floor($diff / 3600);
			$diff = $diff - $hours * 3600;
			$minutes = floor($diff / 60);
			$seconds = $diff - $minutes * 60;

			if ($formatReturn == 'H:i') {
				$out = ' ';
				if ($hours) $out .= " " . str_pad($hours, 2, "0", STR_PAD_LEFT) . ":" . str_pad($minutes, 2, "0", STR_PAD_LEFT);
				if (!$hours && $minutes) $out .= " 00:" . str_pad($minutes, 2, "0", STR_PAD_LEFT);
				if (!$hours && !$minutes && $seconds) $out .= " " . str_pad($seconds, 2, "0", STR_PAD_LEFT) . " sec";
				return substr($out, 1);

			} elseif ($formatReturn == 'H:i:s') {
				$out = ' ';
				if ($hours) $out .= " ".str_pad($hours, 2, "0", STR_PAD_LEFT).":".str_pad($minutes, 2, "0", STR_PAD_LEFT).":".str_pad($seconds, 2, "0", STR_PAD_LEFT);
				if (!$hours && $minutes) $out .= " 00:".str_pad($minutes, 2, "0", STR_PAD_LEFT).":".str_pad($seconds, 2, "0", STR_PAD_LEFT);
				if (!$hours && !$minutes && $seconds) $out .= " ".str_pad($seconds, 2, "0", STR_PAD_LEFT)." sec";
				return substr($out, 1);

			} elseif ($formatReturn == 'H') {
				return $hours;

			} elseif ($formatReturn == 'D') {
				return ceil($hours/24);

			} else {
				throw new CException('Invalid return format');
			}
		} else {
			return $expiredReturn;
		}
	}

	/**
	 * получить разницу в годах и месяцах от текущей даты
	 * @param string $dta - дата
	 * @return string
	 */
	public static function getSincePeriod($dta)
	{
		# 1) ПЕРЕМЕННЫЕ
		# 1.1) переводим строку в штамп
		$dta = strtotime($dta);
		# 1.2) разница переданной и текущей дат
		$difference = time() - $dta;
		# 1.3) общее кол-во месяцев разницы
		$totalMonths = ceil($difference/2628000);
		# 1.4) кол-во целых лет разницы
		$cntYears = floor($totalMonths/12);
		# 1.5) кол-во целых месяцев разницы
		$cntMonths = $totalMonths - ($cntYears * 12);
		# 1.6) итоговая строка
		$text = '';

		# 2) ВЫЧИСЛЕНИЯ
		# 2.1) если есть целое кол-во лет
		if ($cntYears)
			$text .= "{$cntYears} year".($cntYears > 1 ? 's' : '');
		# 2.2) если есть целое кол-во месяцев
		if ($cntMonths)
		{
			# a) при этом если есть целые года нужно вставить пробел
			if ($text)
				$text .= ' ';
			$text .= "{$cntMonths} month".($cntMonths > 1 ? 's' : '');
		}

		return $text;
	}

	public static function diffInHours($minDta, $maxDta, $round=true)
	{
		if ($minDta > $maxDta) {
			return 0;
		}

		$diff = strtotime($maxDta) - strtotime($minDta);
		return $round ? floor($diff / 3600) : $diff / 3600;
	}

	public static function diffInDays($minDta, $maxDta)
	{
		if (strtotime($minDta) > strtotime($maxDta)) {
			return 0;
		}

		$diff = strtotime($maxDta) - strtotime($minDta);
		return floor($diff / 3600 / 24);
	}

	public static function newsAgoFormat($dta, $is_hint=false)
	{
		$now = strtotime(Yii::app()->dater->format(time(), "Y-m-d H:i:s"));
		$event = strtotime(Yii::app()->dater->format(strtotime($dta), "Y-m-d H:i:s"));
		$yesterday = strtotime(Yii::app()->dater->format(strtotime('yesterday'), "Y-m-d H:i:s"));
		$yesterday2 = strtotime(Yii::app()->dater->format(strtotime('yesterday - 1 day'), "Y-m-d H:i:s"));
		$diffStamp = $now - $event;

		if ($diffStamp == 0) {
			$date = 'just now';
		} elseif ($diffStamp < 60) {
			$date = Yii::t('app', '{n} second ago|{n} seconds ago', $diffStamp);
		} elseif ($diffStamp < 1*60*60) {
			$date = Yii::t('app', '{n} minute ago|{n} minutes ago', intval($diffStamp / 60));
		} elseif ($diffStamp < 24*60*60) {
			$date = Yii::t('app', '{n} hour ago|{n} hours ago', intval($diffStamp / 60 / 60));
		} elseif ($event > $yesterday) {
			$date = Yii::t('app', 'yesterday');
		} elseif ($event > $yesterday2) {
			$date = Yii::t('app', 'two days ago');
		} else {
			$date = Yii::app()->dater->format(strtotime($dta), 'd M \\a\\t H:i');
			$is_hint = false;
		}

		if ($is_hint)
			$date = CHtml::tag('span', array('title'=>Yii::app()->dater->format(strtotime($dta), 'd M \\a\\t H:i')), $date);

		return $date;
	}

	public static function seconds2Hi($arg)
	{
		if ($arg) {
			$minVal = floor($arg / 60);
			$secVal = $arg - $minVal*60;
			$value = str_pad($minVal, 2, '0', STR_PAD_LEFT).':'.str_pad($secVal, 2, '0', STR_PAD_LEFT);
		} else {
			$value = '00:00';
		}
		return $value;
	}

	public static function seconds2His($arg)
	{
		$hourVal = floor(($arg) / 3600);
		$minVal = floor(($arg - $hourVal*3600)/ 60);
		$secVal = round($arg - $hourVal*3600 - $minVal*60);
		return str_pad($hourVal, 2, '0', STR_PAD_LEFT).':'.str_pad($minVal, 2, '0', STR_PAD_LEFT).':'.str_pad($secVal, 2, '0', STR_PAD_LEFT);
	}

	public static function seconds2hm($arg)
	{
		$hourVal = floor(($arg) / 3600);
		$minVal = floor(($arg - $hourVal*3600)/ 60);
		return str_pad($hourVal, 2, '0', STR_PAD_LEFT).':'.str_pad($minVal, 2, '0', STR_PAD_LEFT);
	}

	public static function weakStrtotime($arg = 'this')
	{
//return time() - 179 * 24*60*60;
		if ($arg == 'monday this week') {
			return time() - (date("N")-1) * 24*60*60;
		} elseif ($arg == 'monday last week') {
			return time() - (7 + date("N") - 1) * 24 * 60 * 60;
		} elseif ($arg == 'sunday this week') {
			return time() - (-6 + date("N")-1) * 24*60*60;
		} elseif ($arg == 'sunday last week') {
			return time() - (1 + date("N")-1) * 24*60*60;
		} else {
			throw new CException('Unsupported arg');
		}
	}

	public static function toDateStamp($date)
	{
		return strtotime(date('Y-m-d', strtotime($date)));
	}

	public static function to_days($date)
	{
		$days = (strtotime($date) / (60 * 60 * 24));
		return 719527 + $days;
	}

	public static function formatReactSec($seconds, $is_short=false)
	{
		if ($seconds<3600) $t = Yii::t('app', $is_short ? '{n}&#39;' : '{n} minute|{n} minutes', ceil($seconds/60));
		else $t = Yii::t('app', $is_short ? '{n}h' : '{n} hour|{n} hours', ceil($seconds/3600));
		return $t;
	}

	public static function timeAgoConsts()
	{
		return [
			'sTime' => strtotime(Yii::app()->dater->format(time(), "Y-m-d H:i:s")),
			'sYesterday' => strtotime(Yii::app()->dater->format(strtotime('yesterday'), "Y-m-d H:i:s")),
			'sYesterday2' => strtotime(Yii::app()->dater->format(strtotime('yesterday - 1 day'), "Y-m-d H:i:s")),
		];
	}
}