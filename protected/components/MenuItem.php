<?php

class MenuItem extends CComponent
{
	private $_name;
	private $_attributesSource;
	private $_attributesCache;
	private $_userModel;
	private $_userConfig;
	private static $_commonUserConfig;

	public function __construct($name, $user = false)
	{
		$this->_name = $name;
		if ($user) {
			if (!$user instanceof Users) {
				$this->_userModel = Users::model()->findByPk($user);
				if (empty($this->_userModel)) {
					throw new CException('User model not found');
				}
			} else {
				$this->_userModel = $user;
			}
			if (!self::$_commonUserConfig || self::$_commonUserConfig->id_user != $this->_userModel->id) {
				$this->_userConfig = new DUserConfig();
				$this->_userConfig->id_user = $this->_userModel->id;
				self::$_commonUserConfig = $this->_userConfig;
			} else {
				$this->_userConfig = self::$_commonUserConfig;
			}
		}
	}

	public static function loadItems($args = [], $user = false)
	{
		$result = [];
		foreach ($args as $name) {
			$result[] = new self($name, $user);
		}

		return $result;
	}

	public function setAttribute($name, $value)
	{
		if (!$this->_userConfig) {
			return false;
		}
		$this->_userConfig->set($this->resolveAttrName($name), $value);

		return true;
	}

	public function getUserModel()
	{
		return $this->_userModel;
	}

	public function getAttributes()
	{
		if ($this->_attributesCache === null) {
			$this->_attributesSource = Yii::app()->menuMap->get($this->_name);
			// get stored values
			foreach ($this->_attributesSource as $name => $value) {
				if ($this->_userConfig) {
					$this->_attributesSource[$name] = $this->_userConfig->get($this->resolveAttrName($name), $value);
				} else {
					$this->_attributesSource[$name] = $value;
				}
			}

			// calculate dynamic values
			foreach ($this->_attributesSource as $name => $value) {
				$method = '_dynamic' . ucfirst($name);
				if (method_exists($this, $method)) {
					$this->_attributesSource[$name] = $this->$method($this->_attributesSource[$name]);
				}
			}

			$this->_attributesCache = $this->_attributesSource;
		}

		return $this->_attributesCache;
	}

	public function resolveAttrName($name)
	{
		return 'sysmenu.' . $this->_name . '.' . $name;
	}

	private function _dynamicActive($value)
	{
		return Yii::app()->matchMask->check($value);
	}

	private function _dynamicLiClass($value)
	{
		switch ($this->_name) {
//			case 'mypage':
//				if (Yii::app()->user->checkAccess('prime_features')) $value = 'm-prime-logo';
//				break;
		}

		$additionalClass = $value ? $value : '';
		$activeClass = $this->_attributesSource['active'] ? 'active-menu-item' : '';

		if ($activeClass && $additionalClass) {
			return $activeClass.' '.$additionalClass;
		} else {
			$class = $activeClass.$additionalClass;
			return $class ? $class : null;
		}
	}
}