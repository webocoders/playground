<?php
// test change
$file = (!empty($GLOBALS["argv"]) AND in_array('--local', $GLOBALS["argv"])) ? 'console_dev.php' : 'console_prod.php';

while (($i = array_search('--local', $_SERVER['argv'])) !== false) {
	unset($_SERVER['argv'][$i]);
}

return CMap::mergeArray(
	require(dirname(__FILE__).DIRECTORY_SEPARATOR.$file),
	array(
		'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
		'language' => 'en',
		'aliases'=> array(
			'w' => dirname(__FILE__).DIRECTORY_SEPARATOR.'../components/widgets',
		),
		'preload'=>array('log', 'config'),

		'import'=>array(
			'application.components.*',
			'application.components.behaviors.*',
			'application.components.helpers.*',
			'application.components.validators.*',
			'application.models.*',
			'application.controllers',
			'application.commands.*',
		),

		'behaviors'=>array(
			array(
				'class'=>'application.components.behaviors.BootBehavior',
			),
			'templater'=>'ConsoleApplicationTemplater',
		),

		'components'=>array(
			'request' => array(
				'baseUrl' => 'http://playground.udimi.loc',
			),
			'cache'=>array(
				'class'=>'system.caching.CFileCache',
			),
			'log'=>require('log.php'),
			'widgetFactory'=>array(
				'class'=>'CWidgetFactory',
			),
			'urlManager'=>require('urls.php'),
		),

		'params'=>CMap::mergeArray(
			array(
				'console' => true,
			),
			require('params.php')
		),

		'commandMap'=>array(
			'migrate'=>array(
				'class'=>'system.cli.commands.MigrateCommand',
				'templateFile'=>'application.migrations.template',
			),
		),
	)
);