<?php
class ProfileBuyer extends ActiveRecord
{
	const AFTER_DELIVERY_REFUND_RATE_AVG = 15;

	public function tableName()
	{
		return 'profile_buyer';
	}

	public function relations()
	{
		return array(
			'idUser' => array(self::BELONGS_TO, 'Users', 'id_user'),
		);
	}

	public function rules()
	{
		return [
			['privacy_hide_news_rate', 'boolean', 'on'=>'privacy'],
			['privacy_hide_news_buy', 'boolean', 'on'=>'privacy'],
			['privacy_hide_mas', 'boolean', 'on'=>'privacy'],
		];
	}

	public function attributeLabels()
	{
		return array(
			'is_baned_brokering'=>'Reseller mode',
			'privacy_hide_news_rate'=>'Do not show my name on Udimi news when I post a rating',
			'privacy_hide_news_buy'=>'Do not show my name on Udimi news when I buy a solo',
			'privacy_hide_mas'=>'Do not show my name on Movers and shakers list',
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function generateBrokerKey()
	{
		$this->broker_api_key = MyUtils::generateUniqueStringUid(16, (new self)->tableName(), 'broker_api_key');
	}

}
